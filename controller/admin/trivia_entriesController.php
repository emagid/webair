<?php

class trivia_entriesController extends adminController {
	
	function __construct(){
		parent::__construct("Trivia_Entry", "trivia_entries");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		

		parent::index($params);
	}

	function update(Array $arr = []){
				
		parent::update($arr);
	}

	function update_post(){
				
		parent::update_post();
	}
  
}